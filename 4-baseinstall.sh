mount -L ROOT /mnt
mkdir /mnt/boot
mount -L BOOT /mnt/boot
mkdir /mnt/home
mount -L HOME /mnt/home
#swapon -L SWAP
pacstrap /mnt base base-devel linux linux-firmware dhcpcd nano vim
pacman --root /mnt -S intel-ucode
genfstab -U /mnt > /mnt/etc/fstab