lpass show $1 --field='Private Key' > ~/.ssh/id_rsa
lpass show $1 --field=Passphrase
eval `ssh-agent`
ssh-add ~/.ssh/id_rsa
cd ~
git clone git@gitlab.com:chaapp/dotfiles.git
cd ~/dotfiles
./load-ansible-module-aur-arch.sh
cd ~/dotfiles
./init-submodules.sh
echo "Dotfiles successfully installed"
echo "You can now edit the group_var/all and run_roles.yaml file to your needs and run the ansible-playbook run-roles.yaml with --ask-become-pass"