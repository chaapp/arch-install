pacman -S acpid dbus avahi cups
systemctl enable acpid
systemctl enable avahi-daemon
systemctl enable cups.service
systemctl enable --now systemd-timesyncd.service