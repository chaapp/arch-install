read -p "Enter Computername: " computername
echo $computername > /etc/hostname
echo LANG=de_DE.UTF-8 > /etc/locale.conf
echo "de_DE.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo KEYMAP=de-latin1 > /etc/vconsole.conf
echo FONT=lat9w-16 >> /etc/vconsole.conf
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
echo "nameserver 8.8.8.8" >> cat /etc/resolv.conf
echo "127.0.0.1	localhost.localdomain	localhost" >> /etc/hosts
echo "::1		localhost.localdomain	localhost" >> /etc/hosts
echo "Set root Password"
passwd
pacman -S grub
pacman -S efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=Arch-Linux-grub
grub-mkconfig -o /boot/grub/grub.cfg
exit